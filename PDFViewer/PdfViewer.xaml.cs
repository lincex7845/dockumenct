﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace PDFViewer
{
    /// <summary>
    /// Lógica de interacción para PdfViewer.xaml
    /// </summary>
    public partial class PdfViewer : UserControl
    {
        private string _pdfPath;
        private bool _showToolBar;
        private readonly WinFormPdfHost _winFormPdfHost;

        //--------------------------------------------
        public string PdfPath
        {
            get { return _pdfPath; }
            set { _pdfPath = value; }
        }
        //-------------------------------------------
        public bool ShowToolBar
        {
            get { return _showToolBar; }
            set { _showToolBar = value; }
        }
        //-------------------------------------------
        public PdfViewer()
        {
            InitializeComponent();
            _winFormPdfHost = pdfHost.Child as WinFormPdfHost;
        }
        //-------------------------------------------
        public void LoadFile(string path)
        {
            _pdfPath = path;
            _winFormPdfHost.LoadFile(path);
            _winFormPdfHost.ControlPDF.setZoom(70);
        }
        //-------------------------------------------
        public void NextPage()
        {
            _winFormPdfHost.ControlPDF.gotoNextPage();
        }

        //-------------------------------------------
        public void PreviousPage()
        {
            _winFormPdfHost.ControlPDF.gotoPreviousPage();
        }
        //-------------------------------------------
        public void Zomm(float zommPercent)
        {
            _winFormPdfHost.ControlPDF.setZoom(zommPercent);

        }
        //------------------------------------------------------------
        public void PrimeraPag()
        {
            _winFormPdfHost.ControlPDF.gotoFirstPage();
        }
        //------------------------------------------------------------
        public void ultimaPag()
        {
            _winFormPdfHost.ControlPDF.gotoLastPage();
        }
        //------------------------------------------------------------
        public void PanearFlechaDerecha(float zompanear, float panHorizontal, float panVertical)
        {
            _winFormPdfHost.ControlPDF.setZoomScroll(zompanear, panHorizontal, 50);
        }
        //-------------------------------------------------------------------------------------------
        public void PanearFlechaIzquierda(float zompanear, float panHorizontal, float panVertical)
        {
            _winFormPdfHost.ControlPDF.setZoomScroll(zompanear, panHorizontal, 50);
        }
        //----------------------------------------------------------------------------------------
        public void PanearFlechaArriba(float zompanear, float panHorizontal, float panVertical)
        {
            _winFormPdfHost.ControlPDF.setZoomScroll(zompanear, 50, panVertical);
        }
        //------------------------------------------------------------------------------------------
        public void PanearFlechaAbajo(float zompanear, float panHorizontal, float panVertical)
        {
            _winFormPdfHost.ControlPDF.setZoomScroll(zompanear, 50, panVertical);
        }
        //------------------------------------------------------------

    }
}

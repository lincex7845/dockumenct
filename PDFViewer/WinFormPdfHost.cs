﻿using System;
using System.Windows.Forms;

namespace PDFViewer
{
    public partial class WinFormPdfHost : UserControl
    {
        private AxAcroPDFLib.AxAcroPDF axAcroPDF1;

      

        public AxAcroPDFLib.AxAcroPDF ControlPDF
        {
            get { return axAcroPDF1; }
            set { axAcroPDF1 = value; }
        }

        public WinFormPdfHost()
        {
            InitializeComponent();
            if (!DesignMode)
                axAcroPDF1.setShowToolbar(true);
        }

        public void LoadFile(string path)
        {
            axAcroPDF1.LoadFile(path);
            axAcroPDF1.src = path;
            axAcroPDF1.setViewScroll("FitH", 0);
        }

        public void SetShowToolBar(bool on)
        {
            axAcroPDF1.setShowToolbar(on);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinFormPdfHost));
            this.axAcroPDF1 = new AxAcroPDFLib.AxAcroPDF();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).BeginInit();
            this.SuspendLayout();
            // 
            // axAcroPDF1
            // 
            this.axAcroPDF1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axAcroPDF1.Enabled = true;
            this.axAcroPDF1.Location = new System.Drawing.Point(0, 0);
            this.axAcroPDF1.Name = "axAcroPDF1";
            this.axAcroPDF1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDF1.OcxState")));
            this.axAcroPDF1.Size = new System.Drawing.Size(192, 192);
            this.axAcroPDF1.TabIndex = 0;
            // 
            // WinFormPdfHost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.axAcroPDF1);
            this.Name = "WinFormPdfHost";
            this.Size = new System.Drawing.Size(195, 194);
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).EndInit();
            this.ResumeLayout(false);

        }
    }
}

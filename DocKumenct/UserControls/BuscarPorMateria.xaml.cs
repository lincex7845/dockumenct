﻿using System;
using System.Windows;
using System.Windows.Controls;
using DocKumenct.Controles;

namespace DocKumenct.UserControls
{
    /// <summary>
    /// Lógica de interacción para BuscarPorMateria.xaml
    /// </summary>
    public partial class BuscarPorMateria : UserControl
    {
         
        private int isFirst;
        Navegacion nav = null;

        public BuscarPorMateria()
        {
            InitializeComponent();
            nav = Navegacion.InstanciaNav;
        }

        # region eventhandlers

        private void Txt_Subject_GotFocus(object sender, RoutedEventArgs e)
        {
            switch (isFirst)
            {
                case 0:
                    isFirst++;
                    this.Txt_Subject.Text = "  ";
                    break;
                case 1:
                    isFirst++;
                    this.Txt_Subject.Text = "  ";
                    break;
                default:
                    isFirst++;
                    break;
            }
        }

        private void concTexto()
        {
            switch (isFirst)
            {
                case 0:
                    isFirst++;
                    this.Txt_Subject.Text = "  ";
                    break;
                default:
                    isFirst++;
                    break;
            }
        }

        # endregion

        # region keyboard

        private void Btn1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "1";
        }

        private void Btn2_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "2";
        }

        private void Btn3_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "3";
        }

        private void Btn4_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "4";
        }

        private void Btn5_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "5";
        }

        private void Btn6_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "6";
        }

        private void Btn7_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "7";
        }

        private void Btn8_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "8";
        }

        private void Btn9_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "9";
        }

        private void Btn0_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "0";
        }

        private void BtnQ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "Q";
        }

        private void BtnW_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "W";
        }

        private void BtnE_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "E";
        }

        private void BtnR_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "R";
        }

        private void BtnT_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "T";
        }

        private void BtnY_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "Y";
        }

        private void BtnU_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "U";
        }

        private void BtnI_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "I";
        }

        private void BtnO_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "O";
        }

        private void BtnP_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "P";
        }

        private void BtnA_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "A";
        }

        private void BtnS_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "S";
        }

        private void BtnD_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "D";
        }

        private void BtnF_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "F";
        }

        private void BtnG_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "G";
        }

        private void BtnH_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "H";
        }

        private void BtnJ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "J";
        }

        private void BtnK_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "K";
        }

        private void BtnL_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "L";
        }

        private void BtnÑ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "Ñ";
        }

        private void BtnZ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "Z";
        }

        private void BtnX_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "X";
        }

        private void BtnC_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "C";
        }

        private void BtnV_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "V";
        }

        private void BtnB_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "B";
        }

        private void BtnN_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "N";
        }

        private void BtnM_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += "M";
        }

        private void BtnComma_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += ", ";
        }

        private void BtnColon_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += ": ";
        }

        private void BtnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            int len = Txt_Subject.Text.Length;
            if (len > 0)
            {
                Txt_Subject.Text = Txt_Subject.Text.Substring(0, len - 1);
            }
        }

        private void BtnSpace_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Subject.Text += " ";
        }

        # endregion
    }
}

    
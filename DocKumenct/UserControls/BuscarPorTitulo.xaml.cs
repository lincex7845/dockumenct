﻿using System;
using System.Windows;
using System.Windows.Controls;
using DocKumenct.Controles;

namespace DocKumenct.UserControls
{
    /// <summary>
    /// Lógica de interacción para BuscarPorTitulo.xaml
    /// </summary>
    public partial class BuscarPorTitulo : UserControl
    {
       
        private int isFirts;
        Navegacion nav = null;

        public BuscarPorTitulo()
        {
            InitializeComponent();
             nav = Navegacion.InstanciaNav;
        }

        # region eventhandlers       

        private void Txt_Title_GotFocus(object sender, RoutedEventArgs e)
        {
            switch (isFirts)
            {
                case 0:
                    isFirts++;
                    this.Txt_Title.Text = "  ";
                    break;
                case 1:
                    isFirts++;
                    this.Txt_Title.Text = "  ";
                    break;
                default:
                    isFirts++;
                    break;
            }
        }

        private void concTexto()
        {
            switch (isFirts)
            {
                case 0:
                    isFirts++;
                    this.Txt_Title.Text = "  ";
                    break;
                default:
                    isFirts++;
                    break;
            }
        }

        # endregion

        # region keyboard

        private void Btn1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "1";
        }

        private void Btn2_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "2";
        }

        private void Btn3_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "3";
        }

        private void Btn4_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "4";
        }

        private void Btn5_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "5";
        }

        private void Btn6_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "6";
        }

        private void Btn7_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "7";
        }

        private void Btn8_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "8";
        }

        private void Btn9_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "9";
        }

        private void Btn0_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "0";
        }

        private void BtnQ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "Q";
        }

        private void BtnW_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "W";
        }

        private void BtnE_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "E";
        }

        private void BtnR_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "F";
        }

        private void BtnT_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "T";
        }

        private void BtnY_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "Y";
        }

        private void BtnU_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "U";
        }

        private void BtnI_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "I";
        }

        private void BtnO_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "O";
        }

        private void BtnP_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "P";
        }

        private void BtnA_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "A";
        }

        private void BtnS_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "S";
        }

        private void BtnD_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "D";
        }

        private void BtnF_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "F";
        }

        private void BtnG_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "G";
        }

        private void BtnH_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "H";
        }

        private void BtnJ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "J";
        }

        private void BtnK_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "K";
        }

        private void BtnL_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "L";
        }

        private void BtnÑ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "Ñ";
        }

        private void BtnZ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "Z";
        }

        private void BtnX_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "X";
        }

        private void BtnC_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "C";
        }

        private void BtnV_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "V";
        }

        private void BtnB_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "B";
        }

        private void BtnN_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "N";
        }

        private void BtnM_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += "M";
        }

        private void BtnComma_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += ", ";
        }

        private void BtnColon_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += ": ";
        }

        private void BtnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            int len = Txt_Title.Text.Length;
            if (len > 0)
            {
                Txt_Title.Text = Txt_Title.Text.Substring(0, len - 1);
            }
        }

        private void BtnSpace_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            concTexto();
            this.Txt_Title.Text += " ";
        }

        # endregion
    }
}

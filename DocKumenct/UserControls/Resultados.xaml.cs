﻿using System;
using System.Windows;
using DocKumenct.Controles;
using System.Windows.Controls;
using DocKumenct.testDataSetTableAdapters;
using System.Collections.Generic;

namespace DocKumenct.UserControls
{
    /// <summary>
    /// Lógica de interacción para Resultados.xaml
    /// </summary>
    public partial class Resultados : UserControl
    {
        // Table Adapters
        private TesisPorAutor2TableAdapter ata = null;
        private TesisPorMateria2TableAdapter mta = null;
        private TesisPorTitulo2TableAdapter tta = null;
        Navegacion nav = null; 

        private ResultadosTesis list = null;
        int c = 0;
        //public bool bandResults;
        public bool bandVacio;

        public Resultados()
        {
            InitializeComponent();
            nav = Navegacion.InstanciaNav;

            list = ResultadosTesis.Instance;
            list.ThesisResult1 = new List<Tesis>();

            ata = new TesisPorAutor2TableAdapter();
            mta = new TesisPorMateria2TableAdapter();
            tta = new TesisPorTitulo2TableAdapter();
        }

        public void LoadData()
        {
            c = 0;
            if (nav.BandTitulo)
            {
                //Llamar a la funcion buscar por titulo
                testDataSet.TesisPorTitulo2DataTable TTDT = new testDataSet.TesisPorTitulo2DataTable();
                
                int i = tta.Fill(TTDT, nav.TituloB);

               // PENDIENTE 
                if (i == 0)
                {
                    bandVacio = true;
                    list.ThesisResult1.Clear();
                    Tesis T = new Tesis();
                    T.File = "NO EXISTEN DATOS" +".pdf";
                    T.Signature = "NO EXISTEN DATOS";
                    T.Title = "NO EXISTEN DATOS";
                    T.Author = "NO EXISTEN DATOS";
                    list.ThesisResult1.Add(T);

                }
                else
                {
                    bandVacio = false;
                    list.ThesisResult1.Clear();
                    foreach (testDataSet.TesisPorTitulo2Row r in TTDT.Rows)
                    {
                        Tesis T = new Tesis();
                        T.File = "../../Repositorio/" + r[0].ToString() + ".pdf";
                        T.Signature = r[1].ToString() + " " + r[2].ToString();
                        T.Title = r[3].ToString();
                        T.Author = r[4].ToString() + " " + r[5].ToString();
                        list.ThesisResult1.Add(T);
                    }
                    
                }
            }

            else if (nav.BandAutor)
            {
                //Llamar a la funcion buscar Autor
                testDataSet.TesisPorAutor2DataTable TADT = new testDataSet.TesisPorAutor2DataTable();

                int i = ata.Fill(TADT, nav.ApAutorB,nav.NomAutorB);
                 // PENDIENTE 
                if (i == 0)
                {
                    bandVacio = true;
                    list.ThesisResult1.Clear();
                    Tesis T = new Tesis();
                    T.File = "NO EXISTEN DATOS" + ".pdf";
                    T.Signature = "NO EXISTEN DATOS";
                    T.Title = "NO EXISTEN DATOS";
                    T.Author = "NO EXISTEN DATOS";
                    list.ThesisResult1.Add(T);

                }
                else
                {
                    bandVacio = false;

                    list.ThesisResult1.Clear();
                    foreach (testDataSet.TesisPorAutor2Row r in TADT.Rows)
                    {
                        Tesis T = new Tesis();
                        T.File = "../../Repositorio/" + r[0].ToString() + ".pdf";
                        T.Signature = r[1].ToString() + " " + r[2].ToString();
                        T.Title = r[3].ToString();
                        T.Author = r[4].ToString() + " " + r[5].ToString();
                        list.ThesisResult1.Add(T);
                    }
                   
                }


            }

            else if (nav.BandMateria)
            {
                //llamar funcion buscar materia
                testDataSet.TesisPorMateria2DataTable TMDT = new testDataSet.TesisPorMateria2DataTable();

                int i = mta.Fill(TMDT, nav.MateriaB);
                 // PENDIENTE 
                if (i == 0)
                {
                    bandVacio = true;
                    list.ThesisResult1.Clear();
                    Tesis T = new Tesis();
                    T.File = "NO EXISTEN DATOS" + ".pdf";
                    T.Signature = "NO EXISTEN DATOS";
                    T.Title = "NO EXISTEN DATOS";
                    T.Author = "NO EXISTEN DATOS";
                    list.ThesisResult1.Add(T);

                }
                else
                {
                    bandVacio = false;
                    list.ThesisResult1.Clear();
                    foreach (testDataSet.TesisPorMateria2Row r in TMDT.Rows)
                    {
                        Tesis T = new Tesis();
                        T.File = "../../Repositorio/" + r[0].ToString() + ".pdf";
                        T.Signature = r[1].ToString() + " " + r[2].ToString();
                        T.Title = r[3].ToString();
                        T.Author = r[4].ToString() + " " + r[5].ToString();
                        list.ThesisResult1.Add(T);
                    }
                    
                }
            }

            nav.BandVacio = bandVacio;
            this.FillData();

           

            
        }

        private void FillData()
        {
            int max = this.list.ThesisResult1.Count;
            if (this.list.ThesisResult1.Count != 1)
            {
                if (c == 0)
                {
                    this.Btn_Next.Visibility = Visibility.Visible;
                    this.Btn_Previous.Visibility = Visibility.Hidden;
                }
                else if (c == max - 1)
                {
                    this.Btn_Next.Visibility = Visibility.Hidden;
                    this.Btn_Previous.Visibility = Visibility.Visible;
                }
                else
                {
                    this.Btn_Previous.Visibility = Visibility.Visible;
                    this.Btn_Next.Visibility = Visibility.Visible;
                }
            }
            else
            {
                this.Btn_Previous.Visibility = Visibility.Hidden;
                this.Btn_Next.Visibility = Visibility.Hidden;
            }
            Tesis aux = this.list.ThesisResult1[c];
            this.list.SelectedThesis = this.list.ThesisResult1[c];
            //------------------------------------------------------------------
            this.Txt_Title.Text = aux.Title;
            this.Txt_Authors.Text = aux.Author;
            this.Txt_Signatura.Text = aux.Signature;
            //------------------------------------------------------------------
            this.ResultsLabel.Content = "    " + (c + 1) + "/" + max;
        }
        
        private void Btn_Next_Click(object sender, RoutedEventArgs e)
        {
            c++;
            this.FillData();
        }

        private void Btn_Previous_Click(object sender, RoutedEventArgs e)
        {
            c--;
            this.FillData();
        }
    }
}
﻿using System;
using System.Windows;
using System.Windows.Controls;
using DocKumenct.Controles;

namespace DocKumenct.UserControls
{
    /// <summary>
    /// Lógica de interacción para Lector.xaml
    /// </summary>
    public partial class Lector : UserControl
    {
        private ResultadosTesis list = null;
        public bool bandReader;
        float porcentaje = 70;
        float panH;
        float panV;

        public Lector()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            list = ResultadosTesis.Instance;
            this.LoadPDF();
        }

        private void LoadPDF()
        {
            Tesis aux = list.SelectedThesis;
            string path = AppDomain.CurrentDomain.BaseDirectory + @"" + aux.File;
            this.pdfViewer.LoadFile(path);
            
        }

        private void Btn_Home_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void BtnAmpliar_Click(object sender, RoutedEventArgs e)
        {
            panH = panV = 50;
            this.porcentaje += 10;
            this.pdfViewer.Zomm(porcentaje);
            
        }

        private void Btn_reducir_Click(object sender, RoutedEventArgs e)
        {
            panH = panV = 50;
            this.porcentaje -= 10;
            this.pdfViewer.Zomm(porcentaje);
        }
        private void BtnCruzIzq_Click(object sender, RoutedEventArgs e)
        {
            panH -= 50;
            panV += 50;

            this.pdfViewer.PanearFlechaDerecha(porcentaje, panH, panV);
        }

        private void BtnCruzDer_Click(object sender, RoutedEventArgs e)
        {
            panH += 50;
            panV += 50;

            this.pdfViewer.PanearFlechaDerecha(porcentaje, panH, panV);
           
        }

        private void BtnCruzArriba_Click(object sender, RoutedEventArgs e)
        {
            panH += 50;
            panV -= 50;

            this.pdfViewer.PanearFlechaArriba(porcentaje, panH, panV);

        }

        private void BtnCruzAbajo_Click(object sender, RoutedEventArgs e)
        {
            panH += 50;
            panV += 50;

            this.pdfViewer.PanearFlechaAbajo(porcentaje, panH, panV);

        }

        private void BtnPrimeraPag_Click(object sender, RoutedEventArgs e)
        {
            this.pdfViewer.PrimeraPag();
        }

        private void BtnUltimaPagina_Click(object sender, RoutedEventArgs e)
        {
            this.pdfViewer.ultimaPag();
        }

        private void BtnPagSiguiente_Click(object sender, RoutedEventArgs e)
        {
            this.pdfViewer.NextPage();
        }

        private void BtnPagAnterior_Click(object sender, RoutedEventArgs e)
        {
            this.pdfViewer.PreviousPage();

        }

    }
}


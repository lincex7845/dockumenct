﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using DocKumenct.Controles;


namespace DocKumenct.UserControls
{
    /// <summary>
    /// Lógica de interacción para BuscarPorAutor.xaml
    /// </summary>
    public partial class BuscarPorAutor : UserControl
    {
        private bool b;                 //controla que caja de texto tiene foco
        private int isFn;               //bandera para determinar si es la primera vez que TXT_NAME tiene foco
        private int isFln;              //bandera para determinar si es la primera vez que TXT_LASTNAME tiene foco
        //Navegacion nav = null;
        
        public BuscarPorAutor()
        {
            InitializeComponent();
            //nav = Navegacion.InstanciaNav;
            b = true;
            isFn = 0;
            isFln = 0;
        }

        # region eventhandlers

        private void Txt_lastname_GotFocus(object sender, RoutedEventArgs e)
        {
            b = false;
            switch (isFln)
            {
                case 0:
                    isFn++;
                    this.Txt_lastname.Text = "   ";
                    break;
                case 1:
                    isFn++;
                    this.Txt_lastname.Text = "   ";
                    break;
                default:
                    isFn++;
                    break;
            }
        }

        private void Txt_Name_GotFocus(object sender, RoutedEventArgs e)
        {
            b = true;
            switch (isFn)
            {
                case 0:
                    isFn++;
                    this.Txt_Name.Text = "   ";
                    break;
                case 1:
                    isFn++;
                    this.Txt_Name.Text = "   ";
                    break;
                default:
                    isFn++;
                    break;
            }

        }

        private void Validar_Name()
        {
            //b = true;
            switch (isFn)
            {
                case 0:
                    isFn++;
                    this.Txt_Name.Text = "   ";
                    break;
                //case 1:
                //    isFn++;
                //    this.Txt_Name.Text = "   ";
                //    break;
                default:
                    isFn++;
                    break;
            }
        }

        private void Validar_Lastname()
        {
            //b = false;
            switch (isFln)
            {
                case 0:
                    isFln++;
                    this.Txt_lastname.Text = "   ";
                    break;
                //case 1:
                //    isFn++;
                //    this.Txt_lastname.Text = "   ";
                //    break;
                default:
                    isFln++;
                    break;
            }
        }
        
        #endregion

        #region Teclado

        private void Btn1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "1";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "1";
            }
        }

        private void Btn2_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "2";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "2";
            }
        }

        private void Btn3_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "3";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "3";
            }
        }

        private void Btn4_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "4";
            }

            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "4";
            }
        }

        private void Btn5_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "5";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "5";
            }
        }

        private void Btn6_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "6";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "6";
            }
        }

        private void Btn7_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "7";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "7";
            }
        }

        private void Btn8_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "8";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "8";
            }
        }

        private void Btn9_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "9";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "9";
            }
        }

        private void Btn0_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "0";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "0";
            }
        }

        private void BtnQ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "Q";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "Q";
            }
        }

        private void BtnW_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "W";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "W";
            }
        }

        private void BtnE_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "E";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "E";
            }
        }

        private void BtnR_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           if (b)
            {
                Validar_Name();
                Txt_Name.Text += "R";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "R";
            };
        }

        private void BtnT_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "T";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "T";
            }
        }

        private void BtnY_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           if (b)
            {
                Validar_Name();
                Txt_Name.Text += "Y";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "Y";
            }
        }

        private void BtnU_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "U";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "U";
            }
        }

        private void BtnI_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           if (b)
            {
                Validar_Name();
                Txt_Name.Text += "I";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "I";
            }
        }

        private void BtnO_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "O";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "O";
            }
        }

        private void BtnP_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           if (b)
            {
                Validar_Name();
                Txt_Name.Text += "P";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "P";
            }
        }

        private void BtnA_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "A";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "A";
            }
        }

        private void BtnS_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "S";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "S";
            }
        }

        private void BtnD_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           if (b)
            {
                Validar_Name();
                Txt_Name.Text += "D";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "D";
            }
        }

        private void BtnF_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "F";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "F";
            }
        }

        private void BtnG_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           if (b)
            {
                Validar_Name();
                Txt_Name.Text += "G";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "G";
            }
        }

        private void BtnH_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           if (b)
            {
                Validar_Name();
                Txt_Name.Text += "H";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "H";
            }
        }

        private void BtnJ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "J";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "j";
            }
        }

        private void BtnK_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "k";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "k";
            }
        }

        private void BtnL_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "L";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "L";
            }
        }

        private void BtnÑ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "Ñ";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "Ñ";
            }
        }

        private void BtnZ_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "Z";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "Z";
            }
        }

        private void BtnX_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "X";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "X";
            }
        }

        private void BtnC_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "C";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "C";
            }
        }

        private void BtnV_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "V";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "V";
            }
        }

        private void BtnB_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "B";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "B";
            }
        }

        private void BtnN_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "N";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "N";
            }
        }

        private void BtnM_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += "M";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += "M";
            }
        }

        private void BtnComma_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += ",";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += ",";
            }
        }

        private void BtnColon_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += ":";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += ":";
            }
        }

        private void BtnSpace_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                Validar_Name();
                Txt_Name.Text += " ";
            }
            else
            {
                Validar_Lastname();
                Txt_lastname.Text += " ";
            }
        }

        private void BtnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (b)
            {
                int len = Txt_Name.Text.Length;
                if (len > 0)
                {
                    Txt_Name.Text = Txt_Name.Text.Substring(0, len - 1);
                }
            }
            else
            {
                int len = Txt_lastname.Text.Length;
                if (len > 0)
                {
                    Txt_lastname.Text = Txt_lastname.Text.Substring(0, len - 1);
                }
            }
        }

        private void BtnTab_Click(object sender, RoutedEventArgs e)
        {
            if (b)
            {
                Txt_lastname.Focus();
                b = false;


            }

            else
            {
                Txt_Name.Focus();
                b = true;
            }

        }

        # endregion
        
    }
}

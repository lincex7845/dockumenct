﻿#pragma checksum "..\..\..\UserControls\Lector.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2B5AD0DF8884E76DC10A652C22BD1AD8"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.18051
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using PDFViewer;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DocKumenct.UserControls {
    
    
    /// <summary>
    /// Lector
    /// </summary>
    public partial class Lector : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 110 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PDFViewer.PdfViewer pdfViewer;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnCruzArriba;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnCruzAbajo;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnCruzIzq;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnCruzDer;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnAmpliar;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton Btn_reducir;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnPrimeraPag;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnUltimaPagina;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnPagSiguiente;
        
        #line default
        #line hidden
        
        
        #line 177 "..\..\..\UserControls\Lector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Kinect.Toolkit.Controls.KinectTileButton BtnPagAnterior;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DocKumenct;component/usercontrols/lector.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\Lector.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LayoutRoot = ((System.Windows.Controls.Canvas)(target));
            return;
            case 2:
            this.pdfViewer = ((PDFViewer.PdfViewer)(target));
            return;
            case 3:
            this.BtnCruzArriba = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 115 "..\..\..\UserControls\Lector.xaml"
            this.BtnCruzArriba.Click += new System.Windows.RoutedEventHandler(this.BtnCruzArriba_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.BtnCruzAbajo = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 120 "..\..\..\UserControls\Lector.xaml"
            this.BtnCruzAbajo.Click += new System.Windows.RoutedEventHandler(this.BtnCruzAbajo_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.BtnCruzIzq = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 133 "..\..\..\UserControls\Lector.xaml"
            this.BtnCruzIzq.Click += new System.Windows.RoutedEventHandler(this.BtnCruzIzq_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.BtnCruzDer = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 138 "..\..\..\UserControls\Lector.xaml"
            this.BtnCruzDer.Click += new System.Windows.RoutedEventHandler(this.BtnCruzDer_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.BtnAmpliar = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 152 "..\..\..\UserControls\Lector.xaml"
            this.BtnAmpliar.Click += new System.Windows.RoutedEventHandler(this.BtnAmpliar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.Btn_reducir = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 157 "..\..\..\UserControls\Lector.xaml"
            this.Btn_reducir.Click += new System.Windows.RoutedEventHandler(this.Btn_reducir_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.BtnPrimeraPag = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 162 "..\..\..\UserControls\Lector.xaml"
            this.BtnPrimeraPag.Click += new System.Windows.RoutedEventHandler(this.BtnPrimeraPag_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.BtnUltimaPagina = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 167 "..\..\..\UserControls\Lector.xaml"
            this.BtnUltimaPagina.Click += new System.Windows.RoutedEventHandler(this.BtnUltimaPagina_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.BtnPagSiguiente = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 172 "..\..\..\UserControls\Lector.xaml"
            this.BtnPagSiguiente.Click += new System.Windows.RoutedEventHandler(this.BtnPagSiguiente_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.BtnPagAnterior = ((Microsoft.Kinect.Toolkit.Controls.KinectTileButton)(target));
            
            #line 177 "..\..\..\UserControls\Lector.xaml"
            this.BtnPagAnterior.Click += new System.Windows.RoutedEventHandler(this.BtnPagAnterior_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}


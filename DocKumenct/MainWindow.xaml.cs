﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using DocKumenct.Controles;

namespace DocKumenct
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        private const int MinimumScreenWidth = 1920;
        private const int MinimumScreenHeight = 1200;

        private readonly KinectSensorChooser sensorChooser; 
        private Uri sound;
        private SpeechRecognitionEngine sre;

        private KinectSensor sensor;
        public  KinectSensor Sensor
        {
            get { return sensor; }
            set 
            { 
                sensor = value;
                OnPropertyChanged("Sensor");
            }
        }

        private bool conn;
        public bool Conn
        {
            get { return conn; }
            set 
            { 
                conn = value;
                OnPropertyChanged("Conn");
                if (conn)
                    KinectConnected();
                else
                    KinectDisconnected();
            }
        }

        public Navegacion nav = null;        
       
        public MainWindow()
        {
            this.ScreenResolution();
            InitializeComponent(); 

            // initialize the sensor chooser and UI
            this.sensorChooser = new KinectSensorChooser();
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
            this.sensorChooser.Start();
            KinectSensor.KinectSensors.StatusChanged += KinectSensors_StatusChanged;
            // Bind the sensor chooser's current sensor to the KinectRegion
            var regionSensorBinding = new Binding("Kinect") { Source = this.sensorChooser };
            BindingOperations.SetBinding(this.kinectRegion, KinectRegion.KinectSensorProperty, regionSensorBinding);
            Conn = false;

            nav = Navegacion.InstanciaNav;

        }

        private void ScreenResolution()
        {
            // get the main screen size
            double height = SystemParameters.PrimaryScreenHeight;
            double width = SystemParameters.PrimaryScreenWidth;

            // if the main screen is less than 1920 x 1200 then warn the user it is not the optimal experience 

            if ((width < MinimumScreenWidth) || (height < MinimumScreenHeight))
            {
                MessageBoxResult continueResult = MessageBox.Show(Properties.Resources.ResolucionBaja, Properties.Resources.ResolucionBajaTitulo, MessageBoxButton.YesNo);
                if (continueResult == MessageBoxResult.No)
                {
                    this.Close();
                    Application.Current.Shutdown();
                    return;
                }
            }
        }

        #region SpeechRecognition

        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            // Speech utterance confidence below which we treat speech as if it hadn't been heard
            const double ConfidenceThreshold = 0.3;

            if (e.Result.Confidence >= ConfidenceThreshold)
            {
                switch (e.Result.Semantics.Value.ToString())
                {
                    #region TITULO
                    case "TITULO":
                        if (nav.OpenMenu)
                        {
                            Menu.Visibility = Visibility.Visible;
                            Btn_Exit.Visibility = Visibility.Hidden;
                            Exit_label.Visibility = Visibility.Hidden;
                            Btn_Search.Visibility = Visibility.Visible;
                            Search_Label.Visibility = Visibility.Visible;
                            Btn_Home.Visibility = Visibility.Visible;
                            Home_Label.Visibility = Visibility.Visible;
                            buscarPorTitulo.Visibility = Visibility.Visible;
                            OpenSound();
                            nav.OpenMenu = false;
                            nav.OpenTitulo = true;
                        }
                        break;
                    #endregion

                    #region MATERIA
                    case "MATERIA":
                        if (nav.OpenMenu)
                        {
                            Menu.Visibility = Visibility.Visible;
                            Btn_Exit.Visibility = Visibility.Hidden;
                            Exit_label.Visibility = Visibility.Hidden;
                            Btn_Search.Visibility = Visibility.Visible;
                            Search_Label.Visibility = Visibility.Visible;
                            Btn_Home.Visibility = Visibility.Visible;
                            Home_Label.Visibility = Visibility.Visible;
                            buscarPorMateria.Visibility = Visibility.Visible;
                            OpenSound();
                            nav.OpenMenu = false;
                            nav.OpenMateria = true;
                        }
                        break;
                    #endregion

                    #region AUTOR
                    case "AUTOR":
                        if (nav.OpenMenu)
                        {
                            Menu.Visibility = Visibility.Visible;
                            Btn_Exit.Visibility = Visibility.Hidden;
                            Exit_label.Visibility = Visibility.Hidden;
                            Btn_Search.Visibility = Visibility.Visible;
                            Search_Label.Visibility = Visibility.Visible;
                            Btn_Home.Visibility = Visibility.Visible;
                            Home_Label.Visibility = Visibility.Visible;
                            buscarPorAutor.Visibility = Visibility.Visible;
                            OpenSound();
                            nav.OpenMenu = false;
                            nav.OpenAutor = true;
                        }
                        break;
                    #endregion

                    #region INICIO
                    case "INICIO":
                        if (nav.OpenAutor)
                        {
                            buscarPorAutor.Visibility = Visibility.Hidden;
                            Btn_Search.Visibility = Visibility.Hidden;
                            Search_Label.Visibility = Visibility.Hidden;
                            Btn_Home.Visibility = Visibility.Hidden;
                            Home_Label.Visibility = Visibility.Hidden;
                            OpenSound();
                            Btn_Exit.Visibility = Visibility.Visible;
                            Exit_label.Visibility = Visibility.Visible;
                            Menu.Visibility = Visibility.Visible;
                            nav.OpenAutor = false;
                            nav.OpenMenu = true;
                        }
                        else if (nav.OpenMateria)
                        {
                            buscarPorMateria.Visibility = Visibility.Hidden;
                            Btn_Search.Visibility = Visibility.Hidden;
                            Search_Label.Visibility = Visibility.Hidden;
                            Btn_Home.Visibility = Visibility.Hidden;
                            Home_Label.Visibility = Visibility.Hidden;
                            OpenSound();
                            Btn_Exit.Visibility = Visibility.Visible;
                            Exit_label.Visibility = Visibility.Visible;
                            Menu.Visibility = Visibility.Visible;
                            nav.OpenMateria = false;
                            nav.OpenMenu = true;
                        }

                        else if (nav.OpenTitulo)
                        {
                            buscarPorTitulo.Visibility = Visibility.Hidden;
                            Btn_Search.Visibility = Visibility.Hidden;
                            Search_Label.Visibility = Visibility.Hidden;
                            Btn_Home.Visibility = Visibility.Hidden;
                            Home_Label.Visibility = Visibility.Hidden;
                            OpenSound();
                            Btn_Exit.Visibility = Visibility.Visible;
                            Exit_label.Visibility = Visibility.Visible;
                            Menu.Visibility = Visibility.Visible;
                            nav.OpenTitulo = false;
                            nav.OpenMenu = true;
                        }

                        break;
                    #endregion

                    #region BUSCAR
                    case "BUSCAR":
                        if (!nav.OpenMenu && !nav.OpenResultados && !nav.OpenLector)
                        {
                            if (nav.OpenAutor)
                            {
                                nav.OpenAutor = false;
                                nav.BandAutor = true;
                                nav.ApAutorB = buscarPorAutor.Txt_lastname.Text.Trim();
                                nav.NomAutorB = buscarPorAutor.Txt_Name.Text.Trim();
                                buscarPorAutor.Visibility = Visibility.Hidden;
                            }
                            else if (nav.OpenMateria)
                            {
                                nav.OpenMateria = false;
                                nav.MateriaB = buscarPorMateria.Txt_Subject.Text.Trim();
                                nav.BandMateria = true;
                                buscarPorMateria.Visibility = Visibility.Hidden;
                            }

                            else if (nav.OpenTitulo)
                            {
                                nav.OpenTitulo = false;
                                nav.TituloB = buscarPorTitulo.Txt_Title.Text.Trim();
                                nav.BandTitulo = true;
                                buscarPorTitulo.Visibility = Visibility.Hidden;
                            }

                            nav.OpenMenu = false;

                            Btn_Search.Visibility = Visibility.Hidden;
                            Search_Label.Visibility = Visibility.Hidden;
                            Btn_Home.Visibility = Visibility.Hidden;
                            Home_Label.Visibility = Visibility.Hidden;
                            Menu.Visibility = Visibility.Hidden;

                            resultados.LoadData();
                            OpenSound();
                            resultados.Visibility = Visibility.Visible;
                            Btn_Back.Visibility = Visibility.Visible;
                            Back_Label.Visibility = Visibility.Visible;
                            if (nav.BandVacio)
                            {

                                Btn_Read.Visibility = Visibility.Hidden;
                                Read_label.Visibility = Visibility.Hidden;
                            }
                            else
                            {
                                Btn_Read.Visibility = Visibility.Visible;
                                Read_label.Visibility = Visibility.Visible;
                            }
                            nav.OpenResultados = true;

                        }
                     break;
                    #endregion

                    #region ATRAS
                    case "ATRAS":
                         if (!nav.OpenMenu && (!nav.OpenAutor || !nav.OpenMateria || !nav.OpenTitulo))
                         {
                             if (nav.OpenLector)
                             {
                                 nav.OpenLector = false;
                                 Btn_Back.Visibility = Visibility.Hidden;
                                 Back_Label.Visibility = Visibility.Hidden;
                                 lector.Visibility = Visibility.Hidden;
                                 OpenSound();
                                 resultados.Visibility = Visibility.Visible;
                                 Btn_Back.Visibility = Visibility.Visible;
                                 Back_Label.Visibility = Visibility.Visible;
                                 Btn_Read.Visibility = Visibility.Visible;
                                 Read_label.Visibility = Visibility.Visible;

                                 nav.OpenResultados = true;
                             }

                             else if (nav.OpenResultados)
                             {
                                 nav.OpenResultados = false;

                                 resultados.Visibility = Visibility.Hidden;
                                 Btn_Back.Visibility = Visibility.Hidden;
                                 Back_Label.Visibility = Visibility.Hidden;
                                 Btn_Read.Visibility = Visibility.Hidden;
                                 Read_label.Visibility = Visibility.Hidden;

                                 if (nav.BandAutor)
                                 {
                                     nav.BandAutor = false;
                                     OpenSound();
                                     Btn_Search.Visibility = Visibility.Visible;
                                     Search_Label.Visibility = Visibility.Visible;
                                     Btn_Home.Visibility = Visibility.Visible;
                                     Home_Label.Visibility = Visibility.Visible;
                                     buscarPorAutor.Visibility = Visibility.Visible;
                                     nav.OpenAutor = true;
                                 }

                                 if (nav.BandMateria)
                                 {
                                     nav.BandMateria = false;
                                     OpenSound();
                                     Btn_Search.Visibility = Visibility.Visible;
                                     Search_Label.Visibility = Visibility.Visible;
                                     Btn_Home.Visibility = Visibility.Visible;
                                     Home_Label.Visibility = Visibility.Visible;
                                     buscarPorMateria.Visibility = Visibility.Visible;
                                     nav.OpenMateria = true;
                                 }

                                 if (nav.BandTitulo)
                                 {
                                     nav.BandTitulo = false;
                                     OpenSound();
                                     Btn_Search.Visibility = Visibility.Visible;
                                     Search_Label.Visibility = Visibility.Visible;
                                     Btn_Home.Visibility = Visibility.Visible;
                                     Home_Label.Visibility = Visibility.Visible;
                                     buscarPorTitulo.Visibility = Visibility.Visible;
                                     nav.OpenTitulo = true;
                                 }

                             }
                         }
                        break;
                    #endregion

                    #region LEER
                    case "LEER":
                        if (nav.OpenResultados && nav.BandVacio == false)
                        {
                            Btn_Read.Visibility = Visibility.Hidden;
                            Read_label.Visibility = Visibility.Hidden;
                            resultados.Visibility = Visibility.Hidden;
                            nav.OpenResultados = false;

                            OpenSound();
                            lector.LoadData();
                            lector.Visibility = Visibility.Visible;
                            nav.OpenLector = true;
                        }
                        break;
                    #endregion

                    #region CERRAR
                    case "CERRAR":
                        Application.Current.Shutdown();
                        break;
                    #endregion
                }
            }
        }

        private void SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            //ClearRecognitionHighlights();
        }

        private bool InitializeSpeech()
        {
            RecognizerInfo ri = GetKinectRecognizer();
            this.sre = new SpeechRecognitionEngine(ri.Id);
            if (null != ri)
            {
                //Grammars
                var commands = new Choices();
                commands.Add(new SemanticResultValue("Buscar por titulo", "TITULO"));
                commands.Add(new SemanticResultValue("Buscar por autor", "AUTOR"));
                commands.Add(new SemanticResultValue("Buscar por materia", "MATERIA"));
                commands.Add(new SemanticResultValue("cerrar aplicacion", "CERRAR"));
                commands.Add(new SemanticResultValue("Buscar tesis", "BUSCAR"));
                commands.Add(new SemanticResultValue("Inicio", "INICIO"));
                commands.Add(new SemanticResultValue("Atras", "ATRAS"));
                commands.Add(new SemanticResultValue("Leer Tesis", "LEER"));

                var gb = new GrammarBuilder { Culture = ri.Culture };
                gb.Append(commands);
                var g = new Grammar(gb);
                sre.LoadGrammar(g);
                sre.SetInputToAudioStream(
                Sensor.AudioSource.Start(), new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                sre.RecognizeAsync(RecognizeMode.Multiple);
                return true;
            }

            else
            {
                return false;
            }
        }

        private RecognizerInfo GetKinectRecognizer()
        {
            foreach (RecognizerInfo recognizer in SpeechRecognitionEngine.InstalledRecognizers())
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "es-MX".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }
            return null;
        }

        #endregion

        #region Kinect

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {            
            if (args.OldSensor != null)
            {
                try
                {
                    //Sensor = args.OldSensor;
                    args.OldSensor.DepthStream.Range = DepthRange.Default;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch (InvalidOperationException)
                {
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                    Conn = false;
                }
            }

            if (args.NewSensor != null)
            {
                try
                {
                    Sensor = args.NewSensor;
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();
                    Conn = true;
                    try
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Near;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                        args.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                        
                    }
                    catch (InvalidOperationException)
                    {
                        // Non Kinect for Windows devices do not support Near mode, so reset back to default mode.
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }
                }
                catch (InvalidOperationException)
                {
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                    Conn = false;
                }
            }
        }
        
        void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            if (e.Sensor == this.Sensor)
            {
                switch (e.Status)
                {
                    case KinectStatus.Connected:
                    case KinectStatus.DeviceNotGenuine:
                    case KinectStatus.Initializing:
                        break;
                    default:
                        Conn = false;
                        MessageBox.Show(Properties.Resources.DispositivoDesconectado,Properties.Resources.DispositivoDesconectadoTitulo);
                        //Application.Current.Shutdown();
                        break;
                }
            }

        }

        private void KinectConnected()
        {
            BitmapImage Kin = new BitmapImage();
            Kin.BeginInit();
            Kin.UriSource = new Uri(@"/Imagenes/KinectSensor-si.png", UriKind.Relative);
            Kin.EndInit();
            this.kinectStatus.Source = Kin;
            this.kinectStatus.Stretch = System.Windows.Media.Stretch.Uniform;
            this.MicConnected();
        }

        private void KinectDisconnected()
        {
            BitmapImage Kin = new BitmapImage();
            Kin.BeginInit();
            Kin.UriSource = new Uri(@"/Imagenes/KinectSensor-no.png", UriKind.Relative);
            Kin.EndInit();
            this.kinectStatus.Source = Kin;
            this.kinectStatus.Stretch = System.Windows.Media.Stretch.Uniform;
            this.MicDisconnected();
        }

        private void MicDisconnected()
        {
            BitmapImage Mic = new BitmapImage();
            Mic.BeginInit();
            Mic.UriSource = new Uri(@"/Imagenes/Mic-no.png", UriKind.Relative);
            Mic.EndInit();
            this.micStatus.Source = Mic;
            this.micStatus.Stretch = System.Windows.Media.Stretch.Uniform;
        }

        private void MicConnected()
        {
            BitmapImage Mic = new BitmapImage();
            Mic.BeginInit();
            Mic.UriSource = new Uri(@"/Imagenes/Mic-si.png", UriKind.Relative);
            Mic.EndInit();
            this.micStatus.Source = Mic;
            this.micStatus.Stretch = System.Windows.Media.Stretch.Uniform;
        }

        #endregion

        #region Event Handlers

        private void Btn_Title_Click(object sender, RoutedEventArgs e)
        {
            if (nav.OpenMenu)
            {
                Menu.Visibility = Visibility.Visible;
                Btn_Exit.Visibility = Visibility.Hidden;
                Exit_label.Visibility = Visibility.Hidden;
                Btn_Search.Visibility = Visibility.Visible;
                Search_Label.Visibility = Visibility.Visible;
                Btn_Home.Visibility = Visibility.Visible;
                Home_Label.Visibility = Visibility.Visible;
                buscarPorTitulo.Visibility = Visibility.Visible;
                OpenSound();
                nav.OpenMenu = false;
                nav.OpenTitulo = true;
            }
        }

        private void Btn_Subject_Click(object sender, RoutedEventArgs e)
        {
            if (nav.OpenMenu)
            {
                Menu.Visibility = Visibility.Visible;
                Btn_Exit.Visibility = Visibility.Hidden;
                Exit_label.Visibility = Visibility.Hidden;
                Btn_Search.Visibility = Visibility.Visible;
                Search_Label.Visibility = Visibility.Visible;
                Btn_Home.Visibility = Visibility.Visible;
                Home_Label.Visibility = Visibility.Visible;
                buscarPorMateria.Visibility = Visibility.Visible;
                OpenSound();
                nav.OpenMenu = false;
                nav.OpenMateria = true;
            }
        }

        private void Btn_Author_Click(object sender, RoutedEventArgs e)
        {
            if (nav.OpenMenu)
            {
                Menu.Visibility = Visibility.Visible;
                Btn_Exit.Visibility = Visibility.Hidden;
                Exit_label.Visibility = Visibility.Hidden;
                Btn_Search.Visibility = Visibility.Visible;
                Search_Label.Visibility = Visibility.Visible;
                Btn_Home.Visibility = Visibility.Visible;
                Home_Label.Visibility = Visibility.Visible;
                buscarPorAutor.Visibility = Visibility.Visible;
                OpenSound();
                nav.OpenMenu = false;
                nav.OpenAutor = true;
            }
        }

        private void Btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void OpenSound()
        {
            //string Sbutton = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"SndButton.mp3");
            sound = new Uri(@"../../Sonidos/SndButton.mp3", UriKind.Relative);
            //sound = new Uri(Sbutton);
            this.sound1.Source = sound;
            this.sound1.Play();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            
            if (Sensor != null)
            {
                
                if (this.InitializeSpeech())
                {
                    sre.SpeechRecognized += SpeechRecognized;
                    sre.SpeechRecognitionRejected += SpeechRejected;
                    this.MicConnected();
                }
                else
                {
                    this.MicDisconnected();
                    MessageBoxResult errorVozResult = MessageBox.Show(Properties.Resources.ErrorReconocimientoVoz, Properties.Resources.ErrorReconocimientoVozTitulo);
                    
                    Application.Current.Shutdown();
                }
            }
            else
            {
                MessageBox.Show(Properties.Resources.DispositivoDesconectadoCerrar, Properties.Resources.DispositivoDesconectadoTitulo);
                Application.Current.Shutdown();
            }
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Sensor != null)
            {
                sensor.AudioSource.Stop();
                if (sre != null)
                {
                    sre.SpeechRecognized -= SpeechRecognized;
                    sre.SpeechRecognitionRejected -= SpeechRejected;
                    sre.RecognizeAsyncStop();
                }
                this.sensorChooser.KinectChanged -= SensorChooserOnKinectChanged;
                KinectSensor.KinectSensors.StatusChanged -= KinectSensors_StatusChanged;
                this.sensorChooser.Stop();
            }
        }

        private void Btn_Home_Click(object sender, RoutedEventArgs e)
        {
            if (nav.OpenAutor)
            {
                buscarPorAutor.Visibility = Visibility.Hidden;
                Btn_Search.Visibility = Visibility.Hidden;
                Search_Label.Visibility = Visibility.Hidden;
                Btn_Home.Visibility = Visibility.Hidden;
                Home_Label.Visibility = Visibility.Hidden;
                OpenSound();
                Btn_Exit.Visibility = Visibility.Visible;
                Exit_label.Visibility = Visibility.Visible;
                Menu.Visibility = Visibility.Visible;
                nav.OpenAutor = false;
                nav.OpenMenu = true;
            }
            else if (nav.OpenMateria)
            {
                buscarPorMateria.Visibility = Visibility.Hidden;
                Btn_Search.Visibility = Visibility.Hidden;
                Search_Label.Visibility = Visibility.Hidden;
                Btn_Home.Visibility = Visibility.Hidden;
                Home_Label.Visibility = Visibility.Hidden;
                OpenSound();
                Btn_Exit.Visibility = Visibility.Visible;
                Exit_label.Visibility = Visibility.Visible;
                Menu.Visibility = Visibility.Visible;
                nav.OpenMateria = false;
                nav.OpenMenu = true;
            }

            else if (nav.OpenTitulo)
            {
                buscarPorTitulo.Visibility = Visibility.Hidden;
                Btn_Search.Visibility = Visibility.Hidden;
                Search_Label.Visibility = Visibility.Hidden;
                Btn_Home.Visibility = Visibility.Hidden;
                Home_Label.Visibility = Visibility.Hidden;
                OpenSound();
                Btn_Exit.Visibility = Visibility.Visible;
                Exit_label.Visibility = Visibility.Visible;
                Menu.Visibility = Visibility.Visible;
                nav.OpenTitulo = false;
                nav.OpenMenu = true;
            }
        }

        private void Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            
                if (nav.OpenLector)
                {
                    nav.OpenLector = false;
                    Btn_Back.Visibility = Visibility.Hidden;
                    Back_Label.Visibility = Visibility.Hidden;
                    lector.Visibility = Visibility.Hidden;
                    OpenSound();
                    resultados.Visibility = Visibility.Visible;
                    Btn_Back.Visibility = Visibility.Visible;
                    Back_Label.Visibility = Visibility.Visible;
                    Btn_Read.Visibility = Visibility.Visible;
                    Read_label.Visibility = Visibility.Visible;

                    nav.OpenResultados = true;
                }
                else if (nav.OpenResultados)
                {
                    nav.OpenResultados = false;

                    resultados.Visibility = Visibility.Hidden;
                    Btn_Back.Visibility = Visibility.Hidden;
                    Back_Label.Visibility = Visibility.Hidden;
                    Btn_Read.Visibility = Visibility.Hidden;
                    Read_label.Visibility = Visibility.Hidden;

                    if (nav.BandAutor)
                    {
                        nav.BandAutor = false;
                        OpenSound();
                        Btn_Search.Visibility = Visibility.Visible;
                        Search_Label.Visibility = Visibility.Visible;
                        Btn_Home.Visibility = Visibility.Visible;
                        Home_Label.Visibility = Visibility.Visible;
                        buscarPorAutor.Visibility = Visibility.Visible;
                        nav.OpenAutor = true;
                    }

                    if (nav.BandMateria)
                    {
                        nav.BandMateria = false;
                        OpenSound();
                        Btn_Search.Visibility = Visibility.Visible;
                        Search_Label.Visibility = Visibility.Visible;
                        Btn_Home.Visibility = Visibility.Visible;
                        Home_Label.Visibility = Visibility.Visible;
                        buscarPorMateria.Visibility = Visibility.Visible;
                        nav.OpenMateria = true;
                    }

                    if (nav.BandTitulo)
                    {
                        nav.BandTitulo = false;
                        OpenSound();
                        Btn_Search.Visibility = Visibility.Visible;
                        Search_Label.Visibility = Visibility.Visible;
                        Btn_Home.Visibility = Visibility.Visible;
                        Home_Label.Visibility = Visibility.Visible;
                        buscarPorTitulo.Visibility = Visibility.Visible;
                        nav.OpenTitulo = true;
                    }

                }
                //Btn_Exit.Visibility = Visibility.Visible;
                //Exit_label.Visibility = Visibility.Visible;
                //Menu.Visibility = Visibility.Visible;
                //nav.OpenMenu = true;
            
        }

        private void Btn_Search_Click(object sender, RoutedEventArgs e)
        {
            if (nav.OpenAutor)
            { 
                nav.OpenAutor = false;
                nav.BandAutor = true;
                nav.ApAutorB = buscarPorAutor.Txt_lastname.Text.Trim();
                nav.NomAutorB = buscarPorAutor.Txt_Name.Text.Trim();
                buscarPorAutor.Visibility = Visibility.Hidden;
            }
            else if (nav.OpenMateria)
            {                
                nav.OpenMateria = false;
                nav.MateriaB = buscarPorMateria.Txt_Subject.Text.Trim();
                nav.BandMateria = true;
                buscarPorMateria.Visibility = Visibility.Hidden;
            }

            else if (nav.OpenTitulo)
            {
                nav.OpenTitulo = false;
                nav.TituloB = buscarPorTitulo.Txt_Title.Text.Trim();
                nav.BandTitulo = true;
                buscarPorTitulo.Visibility = Visibility.Hidden;
            }

            nav.OpenMenu = false;

            Btn_Search.Visibility = Visibility.Hidden;
            Search_Label.Visibility = Visibility.Hidden;
            Btn_Home.Visibility = Visibility.Hidden;
            Home_Label.Visibility = Visibility.Hidden;
            Menu.Visibility = Visibility.Hidden;

            resultados.LoadData();
            OpenSound();
            resultados.Visibility = Visibility.Visible;
            Btn_Back.Visibility = Visibility.Visible;
            Back_Label.Visibility = Visibility.Visible;
            if (nav.BandVacio)
            {

                Btn_Read.Visibility = Visibility.Hidden;
                Read_label.Visibility = Visibility.Hidden;
            }
            else
            {
                Btn_Read.Visibility = Visibility.Visible;
                Read_label.Visibility = Visibility.Visible;
            }

            nav.OpenResultados = true;

        }

        private void Btn_Read_Click(object sender, RoutedEventArgs e)
        {
            Btn_Read.Visibility = Visibility.Hidden;
            Read_label.Visibility = Visibility.Hidden;            
            resultados.Visibility = Visibility.Hidden;
            nav.OpenResultados = false;

            OpenSound();
            lector.LoadData();
            lector.Visibility = Visibility.Visible;
            nav.OpenLector = true;
        }
        
        #endregion

    }
}


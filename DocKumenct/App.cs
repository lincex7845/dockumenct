﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Threading;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;

namespace DocKumenct
{
    class App : Application
    {
        [STAThread()]
        static void Main()
        {
            Splasher.Splash = new SplashScreenWindow();
            Splasher.ShowSplash();

            for (int i = 0; i < 2000; i++)
            {
                MessageListener.Instance.ReceiveMessage(string.Format("Cargando {0}", i));
                Thread.Sleep(1);
            }
            Splasher.CloseSplash();
            new App();
        }

        public App()
        {
            StartupUri = new System.Uri("MainWindow.xaml", UriKind.Relative);

            Run();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocKumenct.Controles
{
    
    public  class Navegacion
    {
        private static Navegacion instanciaNav = null;
        public static Navegacion InstanciaNav
        {
            get 
            {
                if (instanciaNav == null)
                    instanciaNav = new Navegacion();

                    return Navegacion.instanciaNav; 
                               
            }
            set { Navegacion.instanciaNav = value; }
        }

        private bool openMenu;

        private bool bandVacio;

        public bool BandVacio
        {
            get { return bandVacio; }
            set { bandVacio = value; }
        }

        public bool OpenMenu
        {
            get { return openMenu; }
            set { openMenu = value; }
        }

        private bool openTitulo;
        public bool OpenTitulo
        {
            get { return openTitulo; }
            set { openTitulo = value; }
        }

        private bool openAutor;
        public bool OpenAutor
        {
            get { return openAutor; }
            set { openAutor = value; }
        }

        private bool openMateria;
        public bool OpenMateria
        {
            get { return openMateria; }
            set { openMateria = value; }
        }

        private bool openResultados;
        public bool OpenResultados
        {
            get { return openResultados; }
            set { openResultados = value; }
        }
        
        private bool openLector;
        public bool OpenLector
        {
            get { return openLector; }
            set { openLector = value;}
        }

        private bool bandAutor;

        public bool BandAutor
        {
            get { return bandAutor; }
            set { bandAutor = value; }
        }

        private bool bandMateria;

        public bool BandMateria
        {
            get { return bandMateria; }
            set { bandMateria = value; }
        }

        private bool bandTitulo;

        public bool BandTitulo
        {
            get { return bandTitulo; }
            set { bandTitulo = value; }
        }

        private string apAutorB;

        public string ApAutorB
        {
            get { return apAutorB; }
            set { apAutorB = value; }
        }

        private string nomAutorB;

        public string NomAutorB
        {
            get { return nomAutorB; }
            set { nomAutorB = value; }
        }

        private string materiaB;

        public string MateriaB
        {
            get { return materiaB; }
            set { materiaB = value; }
        }
        private string tituloB;

        public string TituloB
        {
            get { return tituloB; }
            set { tituloB = value; }
        }

        public Navegacion()
        {
            OpenMenu = true;
            openAutor = openTitulo = openMateria = openLector = openResultados = false;
            bandAutor = bandMateria = bandTitulo = false;
        }

    }
}
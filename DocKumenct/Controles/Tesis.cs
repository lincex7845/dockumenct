﻿using System;

namespace DocKumenct.Controles
{
    class Tesis
    {
        private string title;
        private string author;
        private string signature;
        private string file;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string Author
        {
            get { return author; }
            set { author = value; }
        }
        public string Signature
        {
            get { return signature; }
            set { signature = value; }
        }
        public string File
        {
            get { return file; }
            set { file = value; }
        }
    }
}

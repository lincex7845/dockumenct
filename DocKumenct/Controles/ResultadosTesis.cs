﻿using System;
using System.Collections.Generic;

namespace DocKumenct.Controles
{
    class ResultadosTesis
    {
        private static ResultadosTesis instance = null;
        private List<Tesis> thesisResult = null;
        private Tesis selectedThesis = null;

        internal Tesis SelectedThesis
        {
            get
            {
                if (selectedThesis == null)
                {
                    Tesis a1 = new Tesis
                    {
                        Title = "DIDACTICA DEL DISEÑO DE SOFTWARE",
                        Author = "INSUASTY PORTILLA JESUS - JIMENEZ TOLEDO ROBINSON ANDRES - MARTINEZ NAVARRO ALVARO ALEXANDER",
                        Signature = "005.3 I599DI",
                        File = "83284.pdf"
                    };
                    selectedThesis = a1;
                }
                return selectedThesis;
            }
            set { selectedThesis = value; }
        }

        internal List<Tesis> ThesisResult1
        {
            get
            {
                #region is null?
                //if (thesisResult == null)
                //{
                //    //-----------------------------------------------
                //    Tesis a1 = new Tesis
                //    {
                //        Title = "DIDACTICA DEL DISEÑO DE SOFTWARE",
                //        Author = "INSUASTY PORTILLA JESUS - JIMENEZ TOLEDO ROBINSON ANDRES - MARTINEZ NAVARRO ALVARO ALEXANDER",
                //        Signature = "005.3 I599DI",
                //        File = "83284.pdf"
                //    };
                //    //-----------------------------------------------
                //    Tesis a2 = new Tesis
                //    {
                //        Title = "DIONISIO: PROTOTIPO DE MODELADOR DE PROBLEMAS DE SIMPLEX.",
                //        Author = "JAIRO ALBERTO RUANO ARISMENDY - CHRISTIAN HUMBERTO CABRERA JOJOA",
                //        Signature = "001.4 C117",
                //        File = "85023.pdf"
                //    };
                //    //-----------------------------------------------
                //    Tesis a3 = new Tesis
                //    {
                //        Title = "\"AL - KHWARIZMI\" - SISTEMA PARA EL APOYO DE LOS PROCESOS DE ENSEÑANZA - APRENDIZAJE DEL ALGEBRA DE LOS POLINOMIOS UTILIZANDO LA CAJA DE POLINOMIOS, DIRIGIDO A ESTUDIANTES DE EDUCACION SECUNDARIA, BASADO EN LA LUDICA Y EL JUEGO.",
                //        Author = "IVAN MAURICIO BURBANO IGUA - IVAN MAURICIO ARGOTE PUETAMAN",
                //        Signature = "004.67 A693AL",
                //        File = "83474.pdf"
                //    };
                //    //-----------------------------------------------
                //    Tesis a4 = new Tesis
                //    {
                //        Title = "ASESORIA DE DISEÑO INDUSTRIAL EN EL PROGRAMA DE DESARROLLO EMPRESARIAL SECTORIAL - PRODES DE LA ASOCIACION COLOMBIANA DE MEDIANAS Y PEQUEÑAS INDUSTRIAS - ACOPI - SECCIONAL NARIÑO",
                //        Author = "ELIZABETH MEJIA BURBANO",
                //        Signature = "745.2 M516A",
                //        File = "65674.pdf"
                //    };
                //    //-----------------------------------------------
                //    Tesis a5 = new Tesis
                //    {
                //        Title = "DISEÑO DE UN ECOPUENTE PEATONAL EN ESPACIOS URBANOS QUE PERMITA LA INTERELACION SENSITIVA CON EL TRANSEUNTE EN SAN JUAN DE PASTO",
                //        Author = "JUAN GUILLERMO JIMENEZ USCATEGUI - PAULA ANDREA MURILLO JARAMILLO - LUIS ADRIAN PONCE MUÑOZ - ANA PATRICIA TIMARAN RIVERA",
                //        Signature = "352.74 J61D",
                //        File = "82002.pdf"
                //    };
                    //-----------------------------------------------
                //    this.thesisResult = new List<Tesis>();
                //    this.thesisResult.Add(a1);
                //    this.thesisResult.Add(a2);
                //    this.thesisResult.Add(a3);
                //    this.thesisResult.Add(a4);
                //    this.thesisResult.Add(a5);
                //    //-----------------------------------------------
                //}
                #endregion
                return thesisResult;
            }
            set
            {
                thesisResult = value;
            }
        }

        internal static ResultadosTesis Instance
        {
            get
            {
                if (instance == null)
                    instance = new ResultadosTesis();
                return ResultadosTesis.instance;
            }
            set { ResultadosTesis.instance = value; }
        }
    }
}
